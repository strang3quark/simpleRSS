class AppConstants:
    APPLICATION_NAME = 'SimpleRSS'
    APPLICATION_VERSION = 0.3

    DATABASE_FILENAME = "database.db3"

    SCREEN_SHARED_STATUS_DONE_UPDATE = "Done updating"

    SCREEN_CATEGORYLIST_BOTTOM_MESSAGE = " q:Quit,ENTER:Open,r:Reload Category,R:Reload All,?:Help"

    SCREEN_FEEDLIST_BOTTOM_MESSAGE = " q:Quit,ENTER:Open,r:Reload,R:Reload All,a:Mark Feed Read,A:Mark All Read"

    SCREEN_ARTICLELIST_BOTTOM_MESSAGE = " q:Back,ENTER:Open,o: Open in Browser,r:Reload,a:Mark Article Read,A:Mark All Read"

    SCREEN_ARTICLE_BOTTOM_MESSAGE = " q:Back,o: Open in Browser"

    KEY_UP = 259
    KEY_DOWN = 258
    KEY_RIGHT = 261
    KEY_LEFT = 260

    KEYS_MOVE_UP = [KEY_UP, ord('k')]
    KEYS_MOVE_DOWN = [KEY_DOWN, ord('j')]
