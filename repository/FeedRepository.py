from typing import List

from core.decorators.Singleton import Singleton
from core.persistence.DatabaseConnection import DatabaseConnection
from model.Feed import Feed


@Singleton
class FeedRepository(object):
    __connection: DatabaseConnection

    def __init__(self) -> None:
        super().__init__()

        self.__connection = DatabaseConnection()

    def get_feed_by_url_hash(self, url_hash: str) -> Feed:
        result = None

        c = self.__connection.cursor()
        query = "SELECT * FROM feeds WHERE urlhash = ?"
        c.execute(query, (url_hash,))
        result = self.__to_model(c.fetchone())
        c.close()

        return result

    def is_feed_exists(self, url_hash: str) -> bool:
        result = False

        c = self.__connection.cursor()
        query = "SELECT COUNT(urlhash) FROM feeds WHERE urlhash = ?"
        c.execute(query, (url_hash,))
        result = c.fetchone()[0] > 0
        c.close()

        return result

    def insert_feed(self, feed: Feed):
        c = self.__connection.cursor()
        query = "INSERT INTO feeds VALUES (?,?,?)"
        c.execute(query, (feed.url_hash, feed.name, feed.error))
        self.__connection.commit()
        c.close()

    def update_feed(self, feed: Feed):
        c = self.__connection.cursor()
        query = "UPDATE feeds SET name = ?, error = ? WHERE urlhash = ?"
        c.execute(query, (feed.name, feed.error, feed.url_hash))
        self.__connection.commit()
        c.close()

    @staticmethod
    def __to_model(value: List):
        if value:
            return Feed(value[0], value[1], value[2])
        else:
            return None
