from typing import List

from core.decorators.Singleton import Singleton
from core.persistence.DatabaseConnection import DatabaseConnection
from model.Article import Article


@Singleton
class ArticleRepository(object):
    __connection: DatabaseConnection

    def __init__(self) -> None:
        super().__init__()

        self.__connection = DatabaseConnection()

    def count_articles(self, feed_url_hash) -> int:
        result = 0

        c = self.__connection.cursor()
        query = "SELECT COUNT(articleid) FROM articles WHERE feed = ?"
        c.execute(query, (feed_url_hash,))
        result = int(c.fetchone()[0])
        c.close()

        return result

    def count_articles_unread(self, feed_url_hash) -> int:
        result = 0

        c = self.__connection.cursor()
        query = "SELECT COUNT(articleid) FROM articles WHERE feed = ? AND viewed = 0"
        c.execute(query, (feed_url_hash,))
        result = int(c.fetchone()[0])
        c.close()

        return result

    def is_article_exists(self, article_url: str):
        result = False

        c = self.__connection.cursor()
        query = "SELECT COUNT(articleid) FROM articles WHERE url = ?"
        c.execute(query, (article_url,))
        result = int(c.fetchone()[0]) > 0
        c.close()

        return result

    def get_articles_by_feed_url_hash(self, feed_url_hash) -> List[Article]:
        result = None

        c = self.__connection.cursor()
        query = "SELECT * FROM articles WHERE feed = ? ORDER BY pubdatetime DESC"
        c.execute(query, (feed_url_hash,))
        raw = c.fetchall()
        c.close()

        result = list(map(self.__to_model, raw))
        return result

    def get_article_by_url(self, article_url: str):
        result = None

        c = self.__connection.cursor()
        query = "SELECT COUNT(articleid) FROM articles WHERE url = ?"
        c.execute(query, (article_url,))
        result = self.__to_model(c.fetchone())
        c.close()

        return result

    def insert_article(self, article: Article):
        c = self.__connection.cursor()
        query = "INSERT INTO articles(feed,url,title,content,pubdatetime,viewed) VALUES (?,?,?,?,?,?)"
        # when inserted we assume that it's a new article, so it's unread
        c.execute(query, (article.feed, article.url, article.title, article.content, article.pub_datetime, 0))
        self.__connection.commit()

    def update_article(self, article: Article):
        c = self.__connection.cursor()
        query = "UPDATE articles SET feed = ?, url = ?, title = ?, content = ?, pubdatetime = ? " \
                "WHERE url = ?"
        # do not update the viewed here
        c.execute(query, (article.feed, article.url, article.title, article.content, article.pub_datetime,
                          article.url))
        self.__connection.commit()

    def update_article_viewed(self, article_url, viewed: bool):
        c = self.__connection.cursor()
        c.execute("UPDATE articles SET viewed = ? WHERE url = ?", (viewed, article_url))
        self.__connection.commit()

    def update_feed_articles_viewed(self, feed_url_hash, viewed: bool):
        c = self.__connection.cursor()
        c.execute("UPDATE articles SET viewed = ? WHERE feed = ?", (viewed, feed_url_hash))
        self.__connection.commit()

    def update_all_articles_viewed(self, viewed: bool):
        c = self.__connection.cursor()
        c.execute("UPDATE articles SET viewed = ?", (viewed,))
        self.__connection.commit()

    @staticmethod
    def __to_model(value: List):
        if value:
            return Article(value[0], value[1], value[2], value[3], value[4], value[5], value[6])
        else:
            return None
