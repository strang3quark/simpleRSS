import os
import webbrowser

from core.decorators.Singleton import Singleton
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.display.Display import Display
from enumerators.ConfigFilePropertiesEnum import ConfigFilePropertiesEnum
from model.ModelAggregations import FeedWithArticles
from service.ArticleService import ArticleService
from service.FeedService import FeedService
from service.RssService import RssService


@Singleton
class FunctionScreenShared(object):

    def __init__(self) -> None:
        super().__init__()

        self.__config = ConfigurationHandler()
        self.__display = Display()

        self.__feed_service = FeedService()
        self.__article_service = ArticleService()
        self.__rss_service = RssService()

    def open_in_browser(self, url):
        browser = self.__config.get_config(ConfigFilePropertiesEnum.BROWSER)

        web_browser_opener = webbrowser

        if browser:
            web_browser_opener = web_browser_opener.get(browser)

        web_browser_opener.open(url, new=2)

    def update_feed(self, feed_url):
        """
        Add articles to database
        """
        if not feed_url:
            return

        self.__display.set_status("Updating: " + feed_url)

        feed_with_articles: FeedWithArticles = self.__rss_service.get_articles(feed_url)

        if feed_with_articles.feed.error > 0:
            self.__display.set_status("Failed to get feed: " + feed_url)

        self.__feed_service.insert_or_update_feed(feed_with_articles.feed)

        # insert feeds into the database
        try:
            for article in feed_with_articles.articles:
                self.__article_service.insert_or_update_article(article)
        except Exception as e:
            self.__display.set_status("Exception: {0}".format(e))
            return

        self.__display.set_status("Updated: " + feed_url)
        return
