import collections
import curses
import os
from typing import List

from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.display.widget.list.ListItem import ListItem
from core.display.widget.list.ListWidget import ListWidget
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.utils.LogWriter import LogWriter
from core.display.Display import Display
from model.ModelAggregations import FeedWithStats
from screens.ArticleListScreen import ArticleListScreen
from screens.HelpScreen import HelpScreen
from screens.FunctionScreenShared import FunctionScreenShared
from service.ArticleService import ArticleService
from service.FeedService import FeedService

FeedListListItemValue = collections.namedtuple(
    "FeedListListItemValue",
    ["feed_name", "feed_url", "article_count", "unread_article_count", "category"]
)


@Singleton
class FeedListScreen(object):

    def __init__(self) -> None:
        super().__init__()

        self.__title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)
        self.__article_service = ArticleService()

        self.__log_writer = LogWriter()
        self.__config = ConfigurationHandler()
        self.__display = Display()
        self.__function_screen_shared = FunctionScreenShared()
        self.__feed_service = FeedService()

        self.__help_screen = HelpScreen()
        self.__article_list_screen = ArticleListScreen()

    def show(self, category=None):
        self.__log_writer.write("In show_feed_list(), category={0}".format(category))

        self.__display.stdscr.clear()

        feed_list_return_keys = [ord('q'), ord('h'), ord('r'), ord('R'), ord('a'), ord('A'), ord('u'), ord('U'), 10,
                                 ord('l'), ord('?')]
        # loop
        while 1:
            feeds = self.get_feed_list(category)

            if len(feeds) == 0:
                self.__display.close()
                print("You need to add feeds to your {0} file.".format(
                    os.path.join(self.__config.get_configuration_folder(), 'urls')))
                return

            self.__display.show_interface(self.__title, AppConstants.SCREEN_FEEDLIST_BOTTOM_MESSAGE)

            list_widget = ListWidget()
            list_widget.pos_y = 1
            list_widget.height = curses.LINES - 3
            list_widget.items = feeds
            list_widget.return_keys = feed_list_return_keys
            list_widget.callback = self.on_list_widget_return
            list_widget.go_to_top()
            list_widget.draw()
            list_widget.wait_for_input()
            break

    def on_list_widget_return(self, list_widget: ListWidget, char: chr, list_item: ListItem):

        if char == ord('q') or char == ord('h'):  # exit app
            list_widget.deactivate()
            return
        elif char == ord('R'):  # pressed R / update all feeds
            for item in list_widget.items:
                if item.value:
                    self.__function_screen_shared.update_feed(item.value.feed_url)
            self.__display.set_status(AppConstants.SCREEN_SHARED_STATUS_DONE_UPDATE)
        elif char == ord('a'):  # mark feed as read
            self.__article_service.update_feed_articles_viewed(list_item.value.feed_url, True)
        elif char == ord('A'):  # mark all read
            self.__article_service.update_all_articles_viewed(True)
        elif char == ord('u'):  # mark feed as NOT read
            self.__article_service.update_feed_articles_viewed(list_item.value.feed_url, False)
        elif char == ord('U'):  # mark all NOT read
            self.__article_service.update_all_articles_viewed(False)
        elif char == ord('?'):  # help
            self.__help_screen.show()
        elif char == 10 or char == ord('l'):
            self.__article_list_screen.show(
                list_item.value.feed_name,
                [list_item.value.feed_url]
            )

        list_widget.items = self.get_feed_list(list_item.value.category)
        self.__display.stdscr.clear()
        self.__display.show_interface(self.__title, AppConstants.SCREEN_FEEDLIST_BOTTOM_MESSAGE)
        list_widget.refresh()

        return

    def get_feed_list(self, category=None):
        url_file = os.path.join(self.__config.get_configuration_folder(), 'urls')

        f = open(url_file, 'r')

        items: List[ListItem] = []

        current_category_name = ""

        if category and category != "All":
            items.append(ListItem(category, None, True, False))

        for url in f.readlines():
            url = url.strip()

            if url and url[0] != "#":
                if url[0] == "=":  # category
                    current_category_name = url[1:-1]
                    if not category or category == "All":
                        items.append(ListItem(current_category_name, None, True, False))
                else:
                    if not category or category == "All" or current_category_name == category:
                        feed_with_stats = self.__feed_service.get_feed_with_stats(url)

                        if feed_with_stats:
                            items.append(ListItem(
                                self.__name_from_feed_with_stats(feed_with_stats),
                                FeedListListItemValue(
                                    feed_with_stats.feed.name,
                                    url,
                                    feed_with_stats.article_count,
                                    feed_with_stats.unread_article_count,
                                    category
                                ),
                                feed_with_stats.unread_article_count > 0,
                                True
                            ))

        f.close()

        return items

    @staticmethod
    def __name_from_feed_with_stats(feed_with_stats: FeedWithStats) -> str:
        if feed_with_stats.feed.error == 1:
            indicator = "E"
        else:
            indicator = " "

        return "{3} ({0}/{1})\t{2}".format(
            str(feed_with_stats.unread_article_count).zfill(2),
            str(feed_with_stats.article_count).zfill(2),
            feed_with_stats.feed.name, indicator
        )
