from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.display.widget.DocumentWidget import DocumentWidget
from core.utils.LogWriter import LogWriter
from core.utils.RssUtils import RssUtils
from core.display.Display import Display
from screens.FunctionScreenShared import FunctionScreenShared
from screens.HelpScreen import HelpScreen
from service.ArticleService import ArticleService


@Singleton
class ArticleScreen(object):

    def __init__(self) -> None:
        super().__init__()

        self.__title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)
        self.__article_service = ArticleService()

        self.__log_writer = LogWriter()
        self.__display = Display()
        self.__function_screen_shared = FunctionScreenShared()

        self.__help_screen = HelpScreen()

    def show(self, feed_name, article_url, article_content):
        show_article_pad_y = 0
        self.__display.show_interface("{0} - {1}".format(self.__title, feed_name),
                                      AppConstants.SCREEN_ARTICLE_BOTTOM_MESSAGE)

        self.__article_service.update_article_viewed(article_url, True)

        while 1:
            show_article_key, show_article_pad_y = DocumentWidget().draw(
                RssUtils.html_to_text(article_content), show_article_pad_y,
                AppConstants.KEYS_MOVE_UP, AppConstants.KEYS_MOVE_DOWN,
                [ord('q'), ord('h'), ord('l'), ord('o'), ord('u'), ord('?')]
            )

            if show_article_key == ord('q') or show_article_key == ord('h'):
                break
            elif show_article_key == ord('o') or show_article_key == ord('l'):
                self.__function_screen_shared.open_in_browser(article_url)
            elif show_article_key == ord('u'):  # mark article NOT read
                self.__article_service.update_article_viewed(article_url, False)
            elif show_article_key == ord('?'):  # help
                self.__help_screen.show()
        return
