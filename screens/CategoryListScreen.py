import collections
import curses
import os
from dataclasses import dataclass
from typing import List

from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.display.widget.list.ListItem import ListItem
from core.display.widget.list.ListWidget import ListWidget
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.utils.LogWriter import LogWriter
from core.display.Display import Display
from enumerators.PageModeEnum import PageModeEnum
from screens.ArticleListScreen import ArticleListScreen
from screens.FeedListScreen import FeedListScreen
from screens.FunctionScreenShared import FunctionScreenShared
from screens.HelpScreen import HelpScreen
from service.FeedService import FeedService


@dataclass
class CategoryListListItemValue(object):

    def __init__(self, name: str, count_articles: int, count_articles_unread: int) -> None:
        super().__init__()

        self.name = name
        self.count_articles = count_articles
        self.count_articles_unread = count_articles_unread
        self.feed_urls: List[str] = []


@Singleton
class CategoryListScreen(object):

    def __init__(self) -> None:
        super().__init__()

        self.__title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)

        self.__log_writer = LogWriter()
        self.__config = ConfigurationHandler()
        self.__display = Display()
        self.__function_screen_shared = FunctionScreenShared()

        self.__feed_service = FeedService()

        self.__feed_list_screen = FeedListScreen()
        self.__help_screen = HelpScreen()
        self.__article_list_screen = ArticleListScreen()

        self.__mode = PageModeEnum.CATEGORIES_FEEDS

    def show(self, mode: PageModeEnum = PageModeEnum.CATEGORIES_FEEDS):
        self.__log_writer.write("In showCategoryList, mode: {0}".format(mode.value))

        self.__mode = mode

        category_list_return_keys = [ord('q'), ord('r'), ord('R'), ord('h'), ord('l'), ord('?'), 10]

        while 1:
            categories = self.__get_categories_list()

            if len(categories) == 0:
                self.__feed_list_screen.show()

            self.__display.show_interface(self.__title, AppConstants.SCREEN_CATEGORYLIST_BOTTOM_MESSAGE)

            list_widget = ListWidget()
            list_widget.pos_y = 1
            list_widget.height = curses.LINES - 3
            list_widget.items = categories
            list_widget.return_keys = category_list_return_keys
            list_widget.callback = self.on_list_widget_return
            list_widget.go_to_top()
            list_widget.draw()
            list_widget.wait_for_input()
            break

    def on_list_widget_return(self, list_widget: ListWidget, char: chr, list_item: ListItem):
        if char == ord('q') or char == ord('h'):
            list_widget.deactivate()
            return
        elif char == ord('r'):
            for feed in list_item.value.feed_urls:
                self.__function_screen_shared.update_feed(feed)
            self.__display.set_status(AppConstants.SCREEN_SHARED_STATUS_DONE_UPDATE)
        elif char == ord('R'):
            all_category = list_widget.items[1]
            for feed in all_category.value.feed_urls:
                self.__function_screen_shared.update_feed(feed)
            self.__display.set_status(AppConstants.SCREEN_SHARED_STATUS_DONE_UPDATE)
        elif char == ord('?'):
            self.__help_screen.show()
        elif char == ord('l') or char == 10:
            if self.__mode == PageModeEnum.CATEGORIES_FEEDS:
                self.__feed_list_screen.show(list_item.value.name)
            elif self.__mode == PageModeEnum.CATEGORIES_ARTICLES:
                self.__article_list_screen.show(list_item.value.name, list_item.value.feed_urls)

        self.__display.stdscr.clear()
        self.__display.show_interface(self.__title, AppConstants.SCREEN_CATEGORYLIST_BOTTOM_MESSAGE)
        list_widget.refresh()

    def __get_categories_list(self):
        url_file = os.path.join(self.__config.get_configuration_folder(), 'urls')

        items: List[ListItem] = [ListItem("Categories", None, True, False)]

        all_category = self.__create_category_object("All")
        items.append(all_category)

        f = open(url_file, 'r')

        current_category = all_category

        for url in f.readlines():
            url = url.strip()
            if url:
                if url[0] == "=":  # is a category
                    category_name = url[1:-1]
                    current_category = self.__create_category_object(category_name)
                    items.append(current_category)
                else:  # is a feed
                    feed_with_stats = self.__feed_service.get_feed_with_stats(url)

                    current_category.value.feed_urls.append(url)
                    all_category.value.feed_urls.append(url)

                    if feed_with_stats:
                        feed_unread_article_count = feed_with_stats.unread_article_count
                        feed_total_article_count = feed_with_stats.article_count

                        current_category.value.count_articles += feed_total_article_count
                        current_category.value.count_articles_unread += feed_unread_article_count

        # update category names and all count
        total_articles = 0
        total_unread_articles = 0
        for item in items:
            if item.value:
                item.text = self.__item_text(item.value)
                total_articles += item.value.count_articles
                total_unread_articles += item.value.count_articles_unread
                item.bold = item.value.count_articles_unread > 0

        all_category.bold = total_unread_articles > 0
        all_category.value.count_articles = total_articles
        all_category.value.count_articles_unread = total_unread_articles
        all_category.text = self.__item_text(all_category.value)

        return items

    @staticmethod
    def __create_category_object(category_name: str):
        current_category_value = CategoryListListItemValue(category_name, 0, 0)
        return ListItem(category_name, current_category_value, False, True)

    @staticmethod
    def __item_text(category_value: CategoryListListItemValue):
        count = "({0}/{1})".format(category_value.count_articles_unread, category_value.count_articles)
        return "{0}\t{1}".format(count.ljust(8), category_value.name)
