from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.display.widget.DocumentWidget import DocumentWidget
from core.utils.RssUtils import RssUtils
from core.display.Display import Display


@Singleton
class HelpScreen(object):

    def __init__(self) -> None:
        super().__init__()

        self.__title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)

        self.__display = Display()

    def show(self):
        self.__display.show_interface("{0} - Help".format(self.__title), "q - Quit")
        help_content = """<h1>Keys</h1><h2>Feed List</h2><ul>
                         <li>j,Down Arrow - Select next item</li>
                         <li>k,Up Arrow     - Select previous item</li>
                         <li>l, Enter   - Enter feed</li>
                         <li>h, q       - Quit</li>
                         <li>a          - Mark selected feed as read</li>
                         <li>A          - Mark all feeds read</li>
                         <li>r          - Reload selected feed</li>
                         <li>R          - Reload all feeds</li>
                         <li>u          - Mark selected feed as unread</li>
                         <li>U          - Mark all feeds as unread</li>
                         </ul>
                         <h2>Article List</h2>
                         <ul>
                         <li>j,Down Arrow - Select next item</li>
                         <li>k,Up Arrow     - Select previous item</li>
                         <li>l, Enter   - Show article</li>
                         <li>h, q       - Go back to Feed List</li>
                         <li>a          - Mark selected article as read</li>
                         <li>A          - Mark feed articles in read</li>
                         <li>r          - Reload this feed</li>
                         <li>o          - Open this article in browser</li>
                         <li>u          - Mark selected article as unread</li>
                         <li>U          - Mark all feed articles as unread</li>
                         </ul>
                         <h2>Article Content</h2>
                         <ul>
                         <li>j,Down Arrow - Select next item</li>
                         <li>k,Up Arrow     - Select previous item</li>
                         <li>l, Enter   - Show article</li>
                         <li>h, q       - Go back to Article List</li>
                         <li>o          - Open this article in browser</li>
                         <li>u          - Mark this article as unread</li>
                         </ul>"""
        show_article_key, show_article_pad_y = DocumentWidget().draw(RssUtils.html_to_text(help_content),
                                                                           return_keys=[ord('q')])
