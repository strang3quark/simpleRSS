import collections
import curses
from typing import List

from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.display.widget.list.ListItem import ListItem
from core.display.widget.list.ListWidget import ListWidget
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.utils.LogWriter import LogWriter
from core.display.Display import Display
from screens.ArticleScreen import ArticleScreen
from screens.FunctionScreenShared import FunctionScreenShared
from screens.HelpScreen import HelpScreen
from service.ArticleService import ArticleService

ArticleListListItemValue = collections.namedtuple(
    "ArticleListListItemValue",
    ["url", "content", "feed_url", "feed_urls"]
)


@Singleton
class ArticleListScreen(object):

    def __init__(self) -> None:
        super().__init__()

        self.__title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)
        self.__article_service = ArticleService()

        self.__log_writer = LogWriter()
        self.__config = ConfigurationHandler()
        self.__display = Display()
        self.__function_screen_shared = FunctionScreenShared()

        self.__help_screen = HelpScreen()
        self.__article_screen = ArticleScreen()
        self.__feed_name = ""

    def show(self, name, feed_urls):
        self.__feed_name = name

        self.__display.stdscr.clear()

        article_list_return_keys = [ord('q'), ord('h'), ord('r'), ord('R'), ord('a'), ord('A'), ord('u'), ord('U'), 10,
                                    ord('l'), ord('?'), ord('o')]
        while 1:
            articles = self.get_article_list(feed_urls)

            # get out if there's no articles
            if len(articles) == 0:
                break

            self.__display.show_interface(
                "{0} - {1}".format(self.__title, name),
                AppConstants.SCREEN_ARTICLELIST_BOTTOM_MESSAGE
            )

            list_widget = ListWidget()
            list_widget.pos_y = 1
            list_widget.height = curses.LINES - 3
            list_widget.items = articles
            list_widget.return_keys = article_list_return_keys
            list_widget.callback = self.on_list_widget_return
            list_widget.draw()
            list_widget.wait_for_input()
            break

        return

    def on_list_widget_return(self, list_widget: ListWidget, char: chr, list_item: ListItem):
        if char == ord('q') or char == ord('h'):
            list_widget.deactivate()
            return
        elif char == ord('o'):  # open in browser
            self.__function_screen_shared.open_in_browser(list_item.value.url)
            return
        elif char == 10 or char == ord('l'):
            self.__article_screen.show(list_item.text, list_item.value.url, list_item.value.content)
        elif char == ord('?'):  # help
            self.__help_screen.show()
        elif char == ord('r'):  # pressed r / update this feed
            self.__function_screen_shared.update_feed(list_item.value.feed_url)
            list_widget.go_to_top()
        elif char == ord('a') and list_item.bold:  # mark article read
            self.__article_service.update_article_viewed(list_item.value.url, True)
        elif char == ord('A'):  # mark feed read
            self.__article_service.update_feed_articles_viewed(list_item.value.feed_url, True)
        elif char == ord('u') and not list_item.bold:  # mark article NOT read
            self.__article_service.update_article_viewed(list_item.value.url, False)
        elif char == ord('U'):  # mark feed NOT read
            self.__article_service.update_feed_articles_viewed(list_item.value.feed_url, False)

        list_widget.items = self.get_article_list(list_item.value.feed_urls)

        self.__display.stdscr.clear()
        self.__display.show_interface(
            "{0} - {1}".format(self.__title, self.__feed_name),
            AppConstants.SCREEN_ARTICLELIST_BOTTOM_MESSAGE
        )
        list_widget.refresh()

    def get_article_list(self, feed_urls: List[str]) -> List[ListItem]:
        items: List[ListItem] = []
        for feed_url in feed_urls:
            articles = self.__article_service.get_articles_by_feed_url(feed_url)

            for article in articles:
                datetime_tuple = article.pub_datetime.split(',')
                pubdate = "{0}/{1}/{2}".format(datetime_tuple[0], datetime_tuple[1], datetime_tuple[2])

                content_header = [
                    "Title: {0}<br>".format(article.title),
                    "Date:  {0} {1}:{2}<br>".format(pubdate, datetime_tuple[3], datetime_tuple[4]),
                    "Link:  {0}<br>".format(article.url), "<hr>"
                ]

                content = "{0}{1}".format(str.join('', content_header), article.content)

                text = "{0}    {1}".format(pubdate, article.title)
                value = ArticleListListItemValue(article.url, content, feed_url, feed_urls)
                bold = not article.viewed

                items.append(ListItem(text, value, bold, True))

        return items
