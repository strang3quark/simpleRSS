CREATE TABLE feeds
(
    urlhash text primary key,
    name    text,
    error   integer DEFAULT 0
);

CREATE TABLE articles
(
    articleid   integer primary key autoincrement,
    feed        text,
    url         text unique,
    title       text,
    content     text,
    pubdatetime text,
    viewed      integer,
    FOREIGN KEY (feed) REFERENCES feeds (urlhash)
);

CREATE TABLE simplerss(
    name    text primary key,
    value   text
);

INSERT INTO simplerss VALUES ('version', '0.3')