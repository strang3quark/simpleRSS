from enum import Enum


class ConfigFilePropertiesEnum(Enum):
    BROWSER = 'browser'

    PAGEMODE = 'pagemode'

    COLOR_TOPBAR = 'color_topbar'
    COLOR_BOTTOMBAR = 'color_bottombar'
    COLOR_LISTITEM = 'color_listitem'
    COLOR_LISTITEM_SELECTED = 'color_listitem_selected'
    COLOR_LISTITEM_UNREAD = 'color_listitem_unread'
    COLOR_LISTITEM_UNREAD_SELECTED = 'color_listitem_unread_selected'
