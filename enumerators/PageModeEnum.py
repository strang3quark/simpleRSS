from enum import Enum

"""
Enumerator for the page mode that the application should have
    
    CATEGORIES_FEEDS = Categories -> Feeds -> Articles -> ArticleContent
    CATEGORIES_ARTICLES = Categories -> Articles -> ArticleContent
    FEEDS_ARTICLES = Feeds -> Articles -> ArticleContent
"""


class PageModeEnum(Enum):
    CATEGORIES_FEEDS = "categories_feeds"
    CATEGORIES_ARTICLES = "categories_articles"
    FEEDS_ARTICLES = "feeds_articles"
