import collections

FeedWithStats = collections.namedtuple("FeedWithStats", [
    "feed", "article_count", "unread_article_count"
])

FeedWithArticles = collections.namedtuple("FeedWithArticles", [
    "feed", "articles"
])
