class Feed(object):
    __url_hash: str
    __name: str
    __error: int

    def __init__(self, url_hash: str, name: str, error: int) -> None:
        super().__init__()

        self.url_hash = url_hash
        self.name = name
        self.error = error

    @property
    def url_hash(self):
        return self.__url_hash

    @url_hash.setter
    def url_hash(self, value: str):
        self.__url_hash = value

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value: str):
        self.__name = value

    @property
    def error(self):
        return self.__error

    @error.setter
    def error(self, value: int):
        self.__error = value
