class Article(object):
    __article_id: int
    __feed: str
    __url: str
    __title: str
    __content: str
    __pub_datetime: str
    __viewed: int

    def __init__(self, article_id: int, feed: str, url: str, title: str, content: str, pub_datetime: str,
                 viewed: str) -> None:
        super().__init__()

        self.article_id = article_id
        self.url = url
        self.feed = feed
        self.title = title
        self.content = content
        self.pub_datetime = pub_datetime
        self.viewed = viewed

    @property
    def article_id(self):
        return self.__article_id

    @article_id.setter
    def article_id(self, value: int):
        self.__article_id = value

    @property
    def feed(self):
        return self.__feed

    @feed.setter
    def feed(self, value: str):
        self.__feed = value

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, value):
        self.__url = value

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = value

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self, value):
        self.__content = value

    @property
    def pub_datetime(self):
        return self.__pub_datetime

    @pub_datetime.setter
    def pub_datetime(self, value):
        self.__pub_datetime = value

    @property
    def viewed(self):
        return self.__viewed

    @viewed.setter
    def viewed(self, value):
        self.__viewed = value
