import curses

from AppConstants import AppConstants
from core.display.widget.list.ListItem import ListItem
from core.display.widget.list.ListWidget import ListWidget
from core.persistence.DatabaseUpdater import DatabaseUpdater
from enumerators.ConfigFilePropertiesEnum import ConfigFilePropertiesEnum
from core.utils.ConfigurationHandler import ConfigurationHandler
from enumerators.PageModeEnum import PageModeEnum
from core.display.Display import Display
import sys
import traceback
from core.utils.LogWriter import LogWriter
from screens.CategoryListScreen import CategoryListScreen
from screens.FeedListScreen import FeedListScreen


class Main(object):
    def __init__(self, debug=False):

        if debug:
            self.log_writer = LogWriter()
        else:
            self.log_writer = LogWriter("/dev/null")

        self.config = ConfigurationHandler()

        DatabaseUpdater().update()

        title = "{0} {1}".format(AppConstants.APPLICATION_NAME, AppConstants.APPLICATION_VERSION)

        display = Display()
        display.set_window_title(title)

        try:
            self.show_first_page()
        except Exception as e:
            display.close()
            print("{0} crashed:".format(AppConstants.APPLICATION_NAME))
            traceback.print_exc()

        display.close()
        return

    def show_first_page(self):

        page_mode = PageModeEnum(self.config.get_config(ConfigFilePropertiesEnum.PAGEMODE))

        if page_mode == PageModeEnum.CATEGORIES_FEEDS or page_mode == PageModeEnum.CATEGORIES_ARTICLES:
            CategoryListScreen().show(page_mode)
        elif page_mode == PageModeEnum.FEEDS_ARTICLES:
            FeedListScreen().show()
        else:
            FeedListScreen().show()


if __name__ == '__main__':
    args = sys.argv
    arg_debug = "--debug" in args
    Main(arg_debug)
