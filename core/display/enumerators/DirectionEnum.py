from enum import IntEnum


class DirectionEnum(IntEnum):
    UP = 1
    DOWN = -1