class ListItem(object):

    def __init__(self, text, value, bold=False, selectable=True) -> None:
        super().__init__()

        self.text = text
        self.value = value
        self.bold = bold
        self.selectable = selectable
