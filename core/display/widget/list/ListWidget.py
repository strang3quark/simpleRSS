import curses
from typing import List

from core.display.Display import Display
from core.display.enumerators.ColorPairEnum import ColorPairEnum
from core.display.widget.list.ListItem import ListItem


class ListWidget(object):
    def __init__(self):
        self.width = curses.COLS
        self.height = curses.LINES
        self.pos_x = 0
        self.pos_y = 0

        self.callback: [ListWidget, chr, ListItem] = None

        self.keys_move_up = [curses.KEY_UP, ord('k')]
        self.keys_move_down = [curses.KEY_DOWN, ord('j')]
        self.return_keys = [10]

        self.items: List[ListItem] = []

        self.__pad = None
        self.__index_selected = 0
        self.__scroll_pos = 0
        self.__active = False

        self.__display = Display()

    def draw(self):
        self.__active = True

        self.__pad = self.__initialize_pad(self.width, len(self.items))
        self.__pad.keypad(True)
        self.__draw_lines()

    def refresh(self):
        """
        Refresh the rows that are currently being displayed
        :return:
        """

        for index in range(self.__scroll_pos, self.__scroll_pos + self.height):
            if index >= len(self.items):
                break

            item = self.items[index]

            # attr_font_weight = self.__assign_item_font_weight(item)
            # attr_color_pair = curses.color_pair(self.__assign_item_color(index, item))
            # self.__pad.chgat(index, 0, -1, attr_font_weight | attr_color_pair)
            self.__draw_line(index, item)

        self.__pad.refresh(
            self.__scroll_pos, 0,
            self.pos_y, self.pos_x,
            self.height, self.width
        )

    def go_to_top(self):
        self.__index_selected = 0
        self.__scroll_pos = 0
        if len(self.items) > 0 and not self.items[0].selectable:
            self.__index_selected = self.__find_nearest_selectable_item_below()

    def wait_for_input(self):
        while self.__active:
            c = self.__pad.getch()
            if c in self.keys_move_down:
                self.__move_down()
            elif c in self.keys_move_up:
                self.__move_up()
            elif c in self.return_keys:
                self.callback(self, c, self.items[self.__index_selected])
            elif c == curses.KEY_RESIZE:  # terminal resized
                self.__resize()

    def deactivate(self):
        """
        Deactivate the widget
        Cancels the wait for input
        :return:
        """
        self.__active = False

    def __draw_lines(self):
        """
        Draws the lines in the screen
        :return:
        """
        for index, item in enumerate(self.items):
            self.__draw_line(index, item)

        self.__display.stdscr.refresh()
        self.__pad.refresh(
            self.__scroll_pos, 0,
            self.pos_y, self.pos_x,
            self.height, self.width
        )

    def __draw_line(self, index: int, item: ListItem):
        """
        Draws an item in a specific line
        :param index: the line in the pad on which the item should be drawn
        :param item: the item to be drawn
        :return:
        """
        filler = " " * (self.width - len(item.text))
        pos_w = 0

        attr_font_weight = self.__assign_item_font_weight(item)
        attr_color_pair = curses.color_pair(self.__assign_item_color(index, item))
        self.__pad.addstr(index, pos_w, "{0}{1}".format(item.text, filler), attr_font_weight | attr_color_pair)

    def __move_down(self):
        """
        Move to the next item on the list
        :return:
        """
        if self.__index_selected < len(self.items):
            self.__index_selected = self.__find_nearest_selectable_item_below()

            if not self.__is_item_visible(self.__index_selected):
                self.__scroll_pos += 1

            self.refresh()

    def __move_up(self):
        """
        Move to the previous item on the list
        :return:
        """
        if self.__index_selected > 0:
            self.__index_selected = self.__find_nearest_selectable_item_above()

            if not self.__is_item_visible(self.__index_selected):
                self.__scroll_pos -= 1

            self.refresh()

    @staticmethod
    def __assign_item_font_weight(item: ListItem):
        """
        Checks for the curses font weight attribute that matches the item's bold property
        :param item: the list item
        :return: the curses attribute
        """
        if item.bold:
            font_weight_attr = curses.A_BOLD
        else:
            font_weight_attr = curses.A_NORMAL

        return font_weight_attr

    def __assign_item_color(self, index: int, item: ListItem):
        """
        Checks for the curses color pair for the current item
        If the index matches the index_selected and the item is selectable it chooses a SELECTED color pair
        :param index: the list item index on the list
        :param item: the list item
        :return:
        """
        if index == self.__index_selected:
            if item.bold and item.selectable:
                color = ColorPairEnum.PAIR_UNREAD_SELECTED
            else:
                color = ColorPairEnum.PAIR_NORMAL_SELECTED
        else:
            if item.bold and item.selectable:
                color = ColorPairEnum.PAIR_UNREAD_UNSELECTED
            else:
                color = ColorPairEnum.PAIR_NORMAL_UNSELECTED

        return color

    def __find_nearest_selectable_item_below(self):
        """
        Finds the selectable item below
        :return: the nearest selectable item index, or the current selected item if none
        """
        nearest = self.__index_selected
        for index in range(self.__index_selected + 1, len(self.items)):
            if self.items[index].selectable:
                nearest = index
                break
        return nearest

    def __find_nearest_selectable_item_above(self):
        """
        Finds the selectable item above
        :return: the nearest selectable item index, or the current selected item if none
        """
        nearest = self.__index_selected
        for index in range(self.__index_selected - 1, -1, -1):
            if self.items[index].selectable:
                nearest = index
                break
        return nearest

    @staticmethod
    def __initialize_pad(width, number_of_items):
        """
        Creates a new pad (List)
        :param width: the width of the list
        :param number_of_items: the number of items on the list
        :return:
        """
        # I have no idea about the +1
        return curses.newpad(number_of_items + 1, width)

    def __is_item_visible(self, index):
        """
        Checks if the item is being displayed on the list
        :param index: the index of the item in the list
        :return: true if it's on the screen, false if it doesn't
        """
        # scroll pos + height must be bigger than index
        # index must be bigger than scroll pos
        return self.__scroll_pos + self.height > index >= self.__scroll_pos

    def __resize(self):
        self.__display.resize_window()
        self.__pad.getch()
        self.width = curses.COLS - 1
        self.height = curses.LINES - 3
        self.refresh()
