import curses

from core.display.Display import Display
from core.display.enumerators.ColorPairEnum import ColorPairEnum


class DocumentWidget(object):

    def __init__(self) -> None:
        super().__init__()

        self.__display = Display()

    def draw(self, content, pad_y=0, move_up_keys=[curses.KEY_UP, ord('k')],
                     move_down_keys=[curses.KEY_DOWN, ord('j')], return_keys=[]):

        content = self.__display.fit_content(content, curses.COLS - 1)
        pad = curses.newpad(len(content) + 1, curses.COLS)
        pad.keypad(1)

        bold_status = False  # is bold attribute active?
        prev_char = 0
        for y in range(0, len(content)):
            pos_x = 0
            for x in range(0, len(content[y])):
                try:
                    # Bold - Text in bold are surrounded by **
                    if content[y][x] == "*":
                        if prev_char == "*":
                            bold_status = not bold_status  # toggle bold status
                            if bold_status:
                                pad.attron(curses.A_BOLD)
                            else:
                                pad.attroff(curses.A_BOLD)
                            pos_x = pos_x - 1  # go one position back to delete the **
                            continue
                    # End Bold

                    prev_char = content[y][x]
                    pad.addch(y, pos_x, content[y][x])
                    pos_x += 1
                except Exception as e:
                    pad.addstr(y, x, '-')
            # end line
            if curses.has_colors():
                pad.chgat(y, 0, -1, curses.color_pair(ColorPairEnum.PAIR_NORMAL_UNSELECTED))

        # fill blank lines to overwrite old content
        if len(content) < curses.LINES - 3:
            for z in range(len(content) + 1, curses.LINES - 2):
                if curses.has_colors():
                    self.__display.stdscr.addstr(z, 0, " " * curses.COLS, curses.color_pair(ColorPairEnum.PAIR_NORMAL_UNSELECTED))
                else:
                    self.__display.stdscr.addstr(z, 0, " " * curses.COLS)

        # fix when window get resized
        if pad_y >= len(content) - curses.LINES + 2:
            pad_y = len(content) - curses.LINES + 2
        if len(content) <= curses.LINES - 3:  # if the content fits the window goto top
            pad_y = 0
        # end fix

        pad.refresh(pad_y, 0, 1, 0, curses.LINES - 3, curses.COLS)
        self.__display.stdscr.refresh()
        while 1:
            c = pad.getch()
            if c in move_up_keys:
                if pad_y > 0:
                    pad_y -= 1

            elif c in move_down_keys:
                if pad_y <= len(content) - curses.LINES + 2:
                    pad_y += 1

            elif c in return_keys:
                return c, pad_y

            elif c == curses.KEY_RESIZE:  # terminal resized
                self.__display.resize_window()
                pad.getch()
                return '0', pad_y

            pad.refresh(pad_y, 0, 1, 0, curses.LINES - 3, curses.COLS)
