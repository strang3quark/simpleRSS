import curses
import os
import traceback

from core.decorators.Singleton import Singleton
from core.display.enumerators.ColorPairEnum import ColorPairEnum
from core.display.utils import ColorPairCreator
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.utils.LogWriter import LogWriter


@Singleton
class Display(object):
    def __init__(self):
        super().__init__()

        self.__logWriter = LogWriter()
        config = ConfigurationHandler()

        self.__logWriter.write("Initializing screen")

        self.stdscr = None
        try:
            # initialize curses
            self.stdscr = curses.initscr()
            curses.start_color()
            if curses.has_colors():
                ColorPairCreator.initialize_color_pairs(curses, config)
            curses.noecho()
            curses.cbreak()
            curses.curs_set(0)
            self.stdscr.keypad(1)
            # these need to go global because we will handle window resize in this module
            self.topMsg = ""
            self.bottomMsg = ""
        except Exception as e:
            # error ocurred, restore terminal
            self.close()
            print(e)
            traceback.print_exc()  # print the exception
        return

    def close(self):
        if self.stdscr is not None:
            self.stdscr.keypad(0)
        curses.echo()
        curses.nocbreak()
        curses.endwin()

    def show_interface(self, top_msg=None, bottom_msg=None):
        if top_msg is None:
            top_msg = self.topMsg
        else:
            self.topMsg = top_msg

        if bottom_msg is None:
            bottom_msg = self.bottomMsg
        else:
            self.bottomMsg = bottom_msg

        top_msg = self.fit_content(top_msg, curses.COLS)[0]
        bottom_msg = self.fit_content(bottom_msg, curses.COLS)[0]
        if curses.has_colors():
            self.stdscr.addstr(0, 0, top_msg + (" " * (curses.COLS - len(top_msg))),
                               curses.color_pair(ColorPairEnum.PAIR_TOPBAR))
            self.stdscr.addstr(curses.LINES - 2, 0, bottom_msg + (" " * (curses.COLS - len(bottom_msg))),
                               curses.color_pair(ColorPairEnum.PAIR_BOTTOMBAR))
            self.set_status("")  # make status bar match normal_unselected color
        else:
            self.stdscr.addstr(0, 0, top_msg + (" " * (curses.COLS - len(top_msg))))
            self.stdscr.addstr(curses.LINES - 2, 0, bottom_msg + (" " * (curses.COLS - len(bottom_msg))))
        self.stdscr.refresh()
        return

    def resize_window(self):
        y, x = self.stdscr.getmaxyx()
        curses.resizeterm(y, x)
        self.stdscr.clear()
        self.show_interface()
        return

    def fit_content(self, content, cols):
        content = ''.join(content)  # convert to string
        result_content = []
        line = ""

        for i in range(0, len(content)):
            line = line + content[i]
            if len(line) == cols or content[i] == "\n" or (content[i] == " " and len(line) > cols - 5):
                result_content.append(line)
                line = ""

        if len(line) > 0:
            result_content.append(line)
        return result_content

    def get_dimensions(self):
        return curses.LINES, curses.COLS

    def set_status(self, message):
        self.stdscr.insstr(curses.LINES - 1, 0, ' ' * curses.COLS,
                           curses.color_pair(ColorPairEnum.PAIR_NORMAL_UNSELECTED))
        self.stdscr.insstr(curses.LINES - 1, 0, str(message), curses.color_pair(ColorPairEnum.PAIR_NORMAL_UNSELECTED));
        self.stdscr.refresh()
        return

    def set_window_title(self, title):
        compatible_terminals = ['xterm', 'rxvt-unicode-256color', 'rxvt-unicode']
        if os.getenv("TERM") in compatible_terminals:
            print("\x1B]0;%s\x07" % title)
        return
