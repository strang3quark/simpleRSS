from core.display.enumerators.ColorPairEnum import ColorPairEnum
from core.utils.ConfigurationHandler import ConfigurationHandler
from enumerators.ConfigFilePropertiesEnum import ConfigFilePropertiesEnum


def initialize_color_pairs(curses, config: ConfigurationHandler):
    __initialize_pair(curses, config, ColorPairEnum.PAIR_TOPBAR,
                      ConfigFilePropertiesEnum.COLOR_TOPBAR, 0, 7)

    __initialize_pair(curses, config, ColorPairEnum.PAIR_BOTTOMBAR,
                      ConfigFilePropertiesEnum.COLOR_BOTTOMBAR, 0, 7)

    __initialize_pair(curses, config, ColorPairEnum.PAIR_NORMAL_UNSELECTED,
                      ConfigFilePropertiesEnum.COLOR_LISTITEM, 7, 0)

    __initialize_pair(curses, config, ColorPairEnum.PAIR_NORMAL_SELECTED,
                      ConfigFilePropertiesEnum.COLOR_LISTITEM_SELECTED, 0, 7)

    __initialize_pair(curses, config, ColorPairEnum.PAIR_UNREAD_UNSELECTED,
                      ConfigFilePropertiesEnum.COLOR_LISTITEM_UNREAD, 1, 0)

    __initialize_pair(curses, config, ColorPairEnum.PAIR_UNREAD_SELECTED,
                      ConfigFilePropertiesEnum.COLOR_LISTITEM_UNREAD_SELECTED, 1, 7)


def __initialize_pair(curses, config: ConfigurationHandler, color_pair_enum: ColorPairEnum,
                      config_property: ConfigFilePropertiesEnum, default_fg: int, default_bg: int):
    config_value = config.get_config(config_property)

    if config_value:
        fg, bg = config_value.split(',')
        curses.init_pair(color_pair_enum, int(fg), int(bg))
    else:
        curses.init_pair(color_pair_enum, default_fg, default_bg)
