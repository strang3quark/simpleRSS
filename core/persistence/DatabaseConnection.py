import os
import sqlite3

from AppConstants import AppConstants
from core.decorators.Singleton import Singleton
from core.utils.ConfigurationHandler import ConfigurationHandler
from core.utils.LogWriter import LogWriter


@Singleton
class DatabaseConnection(object):
    __connection = None
    __config: ConfigurationHandler
    __logWriter: LogWriter

    def __init__(self) -> None:
        super().__init__()

        self.__logWriter = LogWriter()
        self.__config = ConfigurationHandler(self.__logWriter)
        self.__connection = sqlite3.connect(
            os.path.join(self.__config.get_configuration_folder(), AppConstants.DATABASE_FILENAME)
        )

    def connection(self):
        return self.__connection

    def cursor(self):
        return self.__connection.cursor()

    def commit(self):
        return self.__connection.commit()

    def rollback(self):
        return self.__connection.rollback()
