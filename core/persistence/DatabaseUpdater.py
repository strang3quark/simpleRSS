from core.persistence.DatabaseConnection import DatabaseConnection


class DatabaseUpdater(object):

    __connection: DatabaseConnection

    def __init__(self) -> None:
        super().__init__()

        self.__connection = DatabaseConnection()

    def update(self):
        if self.is_database_empty():
            self.execute_sql_file('changelogs/initialize.sql')

    def is_database_empty(self):
        c = self.__connection.cursor()
        c.execute("SELECT name FROM sqlite_master")
        return len(c.fetchall()) == 0

    def execute_sql_file(self, file):
        query = open(file, 'r').read()

        c = self.__connection.cursor()
        c.executescript(query)
        self.__connection.commit()
        c.close()
