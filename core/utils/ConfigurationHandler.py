import os
from shutil import copyfile

from core.decorators.Singleton import Singleton
from core.utils.LogWriter import LogWriter
from enumerators.ConfigFilePropertiesEnum import ConfigFilePropertiesEnum


@Singleton
class ConfigurationHandler(object):
    configs = []

    def __init__(self) -> None:
        super().__init__()

        self.log_writer = LogWriter()

        if not self.__config_file_present():
            self.__create_config_file()

        if not self.__urls_file_present():
            self.__create_urls_file()

        self.configs = self.__read_config_file()
        return

    def get_config(self, config_property: ConfigFilePropertiesEnum) -> str:
        result = None

        str_property = config_property.value

        self.log_writer.write("getting property: {0}".format(str_property))

        if str_property in self.configs:
            result = self.configs[str_property]

        return result

    def get_configuration_folder(self):
        home_folder = os.path.expanduser('~')
        config_folder = os.path.join(home_folder, '.simplerss')
        if not os.path.exists(config_folder):
            os.mkdir(config_folder)

        return config_folder

    def __get_config_file_path(self):
        return os.path.join(self.get_configuration_folder(), 'config')

    def __get_urls_file_path(self):
        return os.path.join(self.get_configuration_folder(), 'urls')

    def __create_urls_file(self):
        self.log_writer.write("Creating urls file in: {0}".format(self.__get_urls_file_path()))
        copyfile('sample/urls', self.__get_urls_file_path())

    def __create_config_file(self):
        self.log_writer.write("Creating config file in: {0}".format(self.__get_config_file_path()))
        copyfile('sample/config', self.__get_config_file_path())

    def __config_file_present(self) -> bool:
        return os.path.exists(self.__get_config_file_path())

    def __urls_file_present(self) -> bool:
        return os.path.exists(self.__get_urls_file_path())

    def __read_config_file(self):
        configs = {}

        config_file_path = self.__get_config_file_path()

        self.log_writer.write("Loading config file: {0}".format(config_file_path))

        if os.path.exists(config_file_path):
            f = open(config_file_path, 'r')
            lines = f.readlines()
            for line in lines:
                if '=' in line and line[0] != "#":
                    config = line.split('=')
                    config[0] = config[0].strip()
                    config[1] = config[1].strip()
                    configs[config[0]] = config[1]
            f.close()

        return configs
