# gets rss feeds and manages the database
import feedparser
import datetime
import html2text

from model.Article import Article


class RssUtils(object):

    @staticmethod
    def get_feed(url):
        d: feedparser.FeedParserDict = feedparser.parse(url)
        try:
            if d.version == '':
                retbozo = -1
                if d.bozo == True:
                    exc = d.bozo_exception
                    retbozo = "Line {0} \r\n Error: {1}".format(exc.getLineNumber(), exc.getMessage())

                return None, None, None
        except Exception as e:
            return None, None, None

        articles = []
        for entry in d.entries:
            article = Article(-1, None, None, None, None, None, None)

            article.title = entry['title']
            article.url = entry['link']

            content = ""
            if 'content' in entry.keys():
                content = entry['content'][0]['value']
            elif 'summary' in entry.keys():
                content = entry['summary']

            article.content = content

            if 'published_parsed' in entry.keys():
                article_date = entry['published_parsed']
            elif 'updated_parsed' in entry.keys():
                article_date = entry['updated_parsed']
            else:
                article_date = datetime.datetime.timetuple(datetime.datetime.now())

            article.pub_datetime = "{0},{1},{2},{3},{4}".format(
                str(article_date[0]),
                str(article_date[1]).zfill(2),
                str(article_date[2]).zfill(2),
                str(article_date[3]).zfill(2),
                str(article_date[4]).zfill(2)
            )

            articles.append(article)

        return d.feed.title, articles, d.version

    @staticmethod
    def html_to_text(content, showImgTags=False):
        parser = html2text.HTML2Text(bodywidth=0)
        parser.images_with_size = showImgTags
        # parser.images_to_alt = not(showImgTags)
        parser.inline_links = False
        markdown = parser.handle(content)

        # remove some chars
        markdown = markdown.replace('&lt;', '<')
        markdown = markdown.replace('&gt;', '>')
        markdown = markdown.replace(' \- ', ' - ')  # dont know why html2text does this
        return markdown


if __name__ == '__main__':
    import sys

    url = sys.argv[1]
    article = int(sys.argv[2])
    title, content, version = RssUtils.get_feed(url)
    if title == -1 and version == -1:
        print("Version Error, bozo:" + str(content))
        sys.exit()
    elif title == -1 and version == -2:
        print("Some unknown Error")
        sys.exit()

    print("Feed")
    print(title)
    print(version)
    # print("Title:",content[0][1])
    # print("Link:",content[0][0])
    # pubDate = content[0][3]
    # print("Date: {0}-{1}-{2} {3}:{4}".format(pubDate[0],pubDate[1],pubDate[2],pubDate[3],pubDate[4]))
    print("Content:")
    print(RssUtils.html_to_text(content[article].content))
