import hashlib
from typing import List

from core.decorators.Singleton import Singleton
from core.utils.LogWriter import LogWriter
from model.Article import Article
from repository.ArticleRepository import ArticleRepository


@Singleton
class ArticleService(object):
    __articleRepository: ArticleRepository
    __logWriter: LogWriter

    def __init__(self) -> None:
        super().__init__()

        self.__articleRepository = ArticleRepository()
        self.__logWriter = LogWriter()

    def count_articles(self, feed_url) -> int:
        feed_url_hash = self.__url_to_md5(feed_url)
        return self.__articleRepository.count_articles(feed_url_hash)

    def count_articles_unread(self, feed_url) -> int:
        feed_url_hash = self.__url_to_md5(feed_url)
        return self.__articleRepository.count_articles_unread(feed_url_hash)

    def get_articles_by_feed_url(self, feed_url) -> List[Article]:
        feed_url_hash = self.__url_to_md5(feed_url)
        return self.__articleRepository.get_articles_by_feed_url_hash(feed_url_hash)

    def insert_or_update_article(self, article: Article):
        if article and not article.url:
            self.__logWriter.write("Cannot store article without URL {0} - {1}".format(
                article.feed, article.title
            ))

        article_exists = self.__articleRepository.is_article_exists(article.url)

        if article_exists:
            self.__articleRepository.update_article(article)
        else:
            self.__articleRepository.insert_article(article)

    def update_article_viewed(self, article_url, viewed: bool):
        self.__articleRepository.update_article_viewed(article_url, viewed)

    def update_feed_articles_viewed(self, feed_url, viewed: bool):
        feed_url_hash = self.__url_to_md5(feed_url)
        self.__articleRepository.update_feed_articles_viewed(feed_url_hash, viewed)

    def update_all_articles_viewed(self, viewed: bool):
        self.__articleRepository.update_all_articles_viewed(viewed)

    @staticmethod
    def __url_to_md5(url: str):
        return hashlib.md5(url.encode('utf-8')).hexdigest()
