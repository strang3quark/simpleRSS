import hashlib

from core.decorators.Singleton import Singleton
from core.utils.LogWriter import LogWriter
from core.utils.RssUtils import RssUtils
from model.Feed import Feed
from model.ModelAggregations import FeedWithArticles


@Singleton
class RssService(object):
    __logWriter: LogWriter

    def __init__(self) -> None:
        super().__init__()

        self.__logWriter = LogWriter()

    def get_articles(self, feed_url):
        feed_name, articles, version = RssUtils.get_feed(feed_url)

        # put the feed_url in the articles
        hashed_url = self.__url_to_md5(feed_url)

        # create the feed object
        error = 0
        if not feed_name and not version:
            self.__logWriter.write("Failed to get feed: {0}".format(feed_url))
            error = 1

        if articles:
            for article in articles:
                article.feed = hashed_url

        feed = Feed(hashed_url, feed_name, error)

        return FeedWithArticles(feed, articles)

    @staticmethod
    def __url_to_md5(url: str):
        return hashlib.md5(url.encode('utf-8')).hexdigest()
