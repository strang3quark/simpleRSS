import hashlib

from core.decorators.Singleton import Singleton
from model.Feed import Feed
from model.ModelAggregations import FeedWithStats
from repository.FeedRepository import FeedRepository
from service.ArticleService import ArticleService


@Singleton
class FeedService(object):
    __feedRepository: FeedRepository
    __articleService: ArticleService

    def __init__(self) -> None:
        super().__init__()

        self.__feedRepository = FeedRepository()
        self.__articleService = ArticleService()

    def get_feed_with_stats(self, url) -> FeedWithStats:
        url_hash = self.__url_to_md5(url)

        feed = self.__feedRepository.get_feed_by_url_hash(url_hash)

        if not feed:
            return None

        article_count = self.__articleService.count_articles(url)
        unread_article_count = self.__articleService.count_articles_unread(url)

        return FeedWithStats(feed, article_count, unread_article_count)

    def insert_or_update_feed(self, feed: Feed):
        db_feed = self.__feedRepository.get_feed_by_url_hash(feed.url_hash)

        if db_feed:
            if not feed.name:
                feed.name = db_feed.name

            self.__feedRepository.update_feed(feed)

        elif feed.name:
            self.__feedRepository.insert_feed(feed)

    @staticmethod
    def __url_to_md5(url: str):
        return hashlib.md5(url.encode('utf-8')).hexdigest()
