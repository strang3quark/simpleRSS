# simpleRSS
## Curses RSS Reader
I know there are a lot of RSS readers out there, I recently decided to learn how to program with curses and also learn how to use git.
My goal is to make something like newsbeuter but with image support and categories.

###Screenshots (v0.2):

**Category List**

![Category List](http://i.imgur.com/kn6BVC3.png)

**All Feeds List**

![All Feeds List](http://i.imgur.com/FPP0opa.png)

**Category Feeds List**

![Category Feeds List](http://i.imgur.com/arrjGYr.png)

**Article List**

![Article List](http://i.imgur.com/13CyctN.png)

**Article Reading 1**

![Article Reading 1](http://i.imgur.com/5QzcTxH.png)

**Article Reading 2**

![Article Reading 2](http://i.imgur.com/PlhvjHn.png)

Dependencies (Python modules):
  * feedparser
  * html2text

If you are using pip you can install dependencies running:
`pip install -r requirements.txt`


**New in this release release:**
- [x] Organize feeds by categories
  - [x] Organize by Feeds -> Articles (Default)
  - [x] Organize by Category -> Feed -> Articles
  - [x] Organize by Category -> Articles (Partialy developed)

**Coming in next releases:**
- [ ] Add articles to favorites
- [ ] Search function to articles/favorites
  - [ ] Search in category
  - [ ] Search in feed
- [ ] Save articles to file
  - [ ] txt (markdown)
  - [ ] html
- [ ] Display Images (this would be hard)

###Configuration
Configuration files are stored in ~/.simplerss/ directory, all files are created the first time you run the program.

You can add feeds simply adding urls to .simplerss/urls, example:
```
=Linux=
https://www.archlinux.org/feed/news
http://distrowatch.com/news/dw.xml
http://www.reddit.com/r/archlinux/.rss
=Nixers Members=
http://strangequark.cf/index.php/feed
http://venam.nixers.net/blog/feed.xml
http://feeds.xero.nu/rss/blog/newest/
http://www.kaashif.co.uk/feeds/all.atom.xml
http://www.maze.io/feed.xml
```
The text under == is the category name and it's use is optional.

By default simpleRSS will open links with xdg-open, if you want to use other browser you can set it on .simplerss/config, example:
```
browser = firefox
```
If you want to change the colors:
```
# Screen Colors
# 0 - Black; 1 - Red; 2 - Green; 3 - Yellow; 4 - Blue;
# 5 - Magenta; 6 - Cyan; 7 - White;
color_topbar = 136,238
color_bottombar = 136,238
color_listitem = 244,0
color_listitem_selected = 244,239
color_listitem_unread = 136,0
color_listitem_unread_selected = 136,239
```
You can also set different view modes:
```
# categories_feeds    = Pages: categories, feeds, articles, articleContent
# categories_articles = Pages: categories, articles, articleContent
# feeds_articles      = Pages: feeds, articles, articleContent
pagemode = categories_feeds
```


###Keys
####Feed List
* j,Down Arrow - Select next item
* k,Up Arrow	 - Select previous item
* l, Enter	 - Enter feed
* h, q		 - Quit
* a			 - Mark selected feed as read
* A			 - Mark all feeds read
* r			 - Reload selected feed
* R			 - Reload all feeds
* u			 - Mark selected feed as unread
* U			 - Mark all feeds as unread

####Article List
* j,Down Arrow - Select next item
* k,Up Arrow	 - Select previous item
* l, Enter	 - Show article
* h, q		 - Go back to Feed List
* a			 - Mark selected article as read
* A			 - Mark feed articles in read
* r			 - Reload this feed
* o			 - Open this article in browser
* u			 - Mark selected article as unread
* U			 - Mark all feed articles as unread

####Article Content
* j,Down Arrow - Select next item
* k,Up Arrow	 - Select previous item
* l, Enter	 - Show article
* h, q		 - Go back to Article List
* o			 - Open this article in browser
* u			 - Mark this article as unread

### Others
Some planed features weren't incorporated in this release, I've postponed it for the next release. The last version was released a long time ago and the features already implemented diserve a new release, also I don't know when I will be able to continue development.
